#include <ncurses.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
int main(int argc, char *argv[])
{
	initscr();
	noecho();
	curs_set(0);
	nodelay(stdscr, TRUE);
	keypad(stdscr, TRUE);
	char memory[0x10000], *rom[0x2000];
	unsigned short pc;
	char a, x, y, status, key;
	unsigned char sp = 0xFF;
	FILE *file = fopen(argv[1], "rb");
	if(file == NULL)
	{
		endwin();
		printf("Error: %s: No such file.\n", argv[1]);
		return 1;
	}
	fread(rom, 1, 0x2000, file);
	fclose(file);
	while(1)
	{
		memcpy(&memory[0xE000], rom, 0x2000);
		switch(memory[pc])
		{
			case (char)0xEA: // nop
				pc++;
				nanosleep((struct timespec[]){0, 2000}, NULL);
				break;
			default:
				pc++;
				nanosleep((struct timespec[]){0, 2000}, NULL);
				break;
		}
		key = getch();
		memory[0xD82F] = key == ERR ? memory[0xD82F] : key;
		printf("\x1B[;H");
		for(unsigned short index = 0xD830; index < 0xE000; index++)
		{
			printf("%c", (memory[index] > 0x1F && memory[index] < 0x7F) || memory[index] > 0xA0 ? memory[index] : ' ');
		}
	}
}